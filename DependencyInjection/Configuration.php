<?php

namespace Melodies\CustomFieldsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('melodies_custom_fields');

        $rootNode
            ->children()
                ->scalarNode('base_path')
                    ->defaultValue('bundles/melodiescustomfields/')
                ->end()
                ->scalarNode('css_path')
                    ->defaultValue('bundles/melodiescustomfields/css/bootstrap.min.css')
                ->end()
                ->scalarNode('js_path')
                    ->defaultValue('bundles/melodiescustomfields/js/bootstrap.min.js')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
