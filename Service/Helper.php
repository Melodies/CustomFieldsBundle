<?php
namespace Melodies\CustomFieldsBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Helper
{
	protected $container;
	protected $orm;

	public function __construct(ContainerInterface $container, $orm)
	{
		$this->container = $container;
		$this->orm       = $orm;
	}

	public function getBloc($idPage)
	{
		$fieldsClean = [];

		if($idPage == null) {
			throw new \Exception('Page id can\'t be null');
		}

		$bloc = $this->orm->getRepository('MelodiesCustomFieldsBundle:Page')->findOneBy(['idPage' => $idPage, 'type' => 'bloc']);

		if($bloc)
		{
			$fields = $this->orm->getRepository('MelodiesCustomFieldsBundle:Field')->findBy(['page' => $bloc->getId()]);

			foreach($fields as $field)
			{
				if($field->getType() == "repeater")
				{
					$id_repeater = $field->getId();
					$a_repeater = [];

					foreach($fields as $sub_field)
					{
						if($id_repeater == $sub_field->getParent() && $sub_field->getPattern() != 0)
						{
							$a_repeater[$sub_field->getPattern()][] = $sub_field->getContent();
						}
					}

					$fieldsClean[$field->getName()] = $a_repeater;
				}
				else
				{
					$fieldsClean[$field->getName()] = $field->getContent();
				}
			}
		}

		return $fieldsClean;
	}

	public function getFields($nameRoute = null)
	{
		$fields = null;
		$route = null;
		$fieldsClean = [];

		$route = ($nameRoute == null)
			? $this->container->get('request')->attributes->get('_route')
			: $nameRoute
		;

		if($route !== null)
		{
			$page = $this->orm->getRepository('MelodiesCustomFieldsBundle:Page')->findOneBy(['idPage' => $route, 'type' => 'page']);

			if($page != null)
			{
				$fields = $this->orm->getRepository('MelodiesCustomFieldsBundle:Field')->findBy(['page' => $page->getId()]);

				foreach($fields as $field)
				{
					if($field->getType() == "repeater")
					{
						$id_repeater = $field->getId();
						$a_repeater = [];

						foreach($fields as $sub_field)
						{
							if($id_repeater == $sub_field->getParent() && $sub_field->getPattern() != 0)
							{
								$a_repeater[$sub_field->getPattern()][] = $sub_field->getContent();
							}
						}

						$fieldsClean[$field->getName()] = $a_repeater;
					}
					else
					{
						$fieldsClean[$field->getName()] = $field->getContent();
					}
				}
			}
		}

		return $fieldsClean;
	}
}