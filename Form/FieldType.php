<?php

namespace Melodies\CustomFieldsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FieldType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', 'text', [
                'label' => 'name'
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Type',
                'choices' => [
                    'textarea' => "Textarea",
                    'text' => "Text",
                    'integer' => "Nombre",
                    'url' => "Url",
                    'choice' => "Checkbox",
                    'ckeditor' => "Wiziwig",
                    'image'   => "Image",
                    'upload' => "Fichier",
                    'repeater' => "Répéteur de sections"
                ]
            ])
            ->add('isNullable', 'checkbox', [
                'label' => 'Can be empty?',
                'required' => false
            ])
            ->add('position','hidden',[
                'label' => 'Position',
                'attr'=> [
                    "hidden" => true,
                ]
            ])
            ->add('content', TextType::class, [
                'label' => 'Position'
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver->setDefaults([
            'data_class' => 'Melodies\CustomFieldsBundle\Entity\Field'
        ]);
    }

    public function getName(){
        return 'melodies_field';
    }
}